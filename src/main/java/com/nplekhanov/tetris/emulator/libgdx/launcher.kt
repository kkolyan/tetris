package com.nplekhanov.tetris.emulator.libgdx

import com.badlogic.gdx.backends.lwjgl.LwjglApplication

fun main(args: Array<String>) {
    val listener = Emulator(16, 8, 16)
    LwjglApplication(listener, listener.config)
}