package com.nplekhanov.tetris.emulator.libgdx

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.nplekhanov.tetris.api.*
import com.nplekhanov.tetris.games.snake.SnakeGame
import com.nplekhanov.tetris.games.tetris.TetrisGame

class Emulator(
        private val cellSize: Int,
        private val columnCount: Int,
        private val rowCount: Int) : ApplicationListener, Device {

    val config = LwjglApplicationConfiguration()

    private var game: GameProcess? = null
    private var selectedGameIndex: Int = 0
    private var games: List<Game> = listOf(
            TetrisGame(),
            SnakeGame()
    )
    private lateinit var shapeRenderer: ShapeRenderer
    private lateinit var font: BitmapFont
    private lateinit var batch: Batch
    private val pixels = hashSetOf<Vec2i>()

    init {
        config.resizable = false
        config.width = cellSize * columnCount
        config.height = cellSize * rowCount
    }

    override fun create() {
        shapeRenderer = ShapeRenderer()
        font = BitmapFont()
        batch = SpriteBatch()
    }

    override fun render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        Gdx.gl.glDisable(GL20.GL_BLEND)

        if (game == null) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
                Gdx.app.exit()
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
                selectedGameIndex--
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
                selectedGameIndex++
            }
            while (selectedGameIndex < 0) {
                selectedGameIndex += games.size
            }
            while (selectedGameIndex >= games.size) {
                selectedGameIndex -= games.size
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                game = games[selectedGameIndex].createProcess(this);
            }

            font.color = Color.GOLD
            batch.begin()
            for ((i, g) in games.withIndex()) {
                font.draw(batch, "${g.name} ${if (selectedGameIndex == i) " *" else ""}", cellSize.toFloat(), cellSize * rowCount - font.lineHeight * (i + 1))
            }
            batch.end()
        } else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
                game = null
            }
        }
        if (game != null) {
            game!!.update(Gdx.app.graphics.deltaTime)
            shapeRenderer.color = if (game!!.isOver()) Color.RED else Color.WHITE

            shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
            repeat(columnCount) { x ->
                repeat(rowCount) { y ->
                    drawCell(x, y)
                }
            }
            shapeRenderer.end()

            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
            for (pixel in pixels) {
                drawCell(pixel.x, pixel.y)
            }
            shapeRenderer.end()

            pixels.clear()
        }
    }

    private fun drawCell(x: Int, y: Int) {
        shapeRenderer.rect(
                x * cellSize.toFloat(),
                y * cellSize.toFloat(),
                cellSize.toFloat(),
                cellSize.toFloat())
    }

    override val width: Int get() = columnCount
    override val height: Int get() = rowCount

    override fun drawPixel(x: Int, y: Int) {
        pixels.add(Vec2i(x, y))
    }

    override fun isButtonPressed(button: Button): Boolean {
        return Gdx.input.isButtonPressed(button2key(button))
    }

    override fun isButtonJustPressed(button: Button): Boolean {
        return Gdx.input.isKeyJustPressed(button2key(button))
    }

    private fun button2key(button: Button): Int {
        return when (button) {
            Button.DOWN -> Input.Keys.DOWN
            Button.UP -> Input.Keys.UP
            Button.LEFT -> Input.Keys.LEFT
            Button.RIGHT -> Input.Keys.RIGHT
        }
    }

    override fun dispose() {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
    }
}