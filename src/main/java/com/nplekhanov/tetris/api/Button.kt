package com.nplekhanov.tetris.api

enum class Button {
    UP, DOWN, LEFT, RIGHT
}