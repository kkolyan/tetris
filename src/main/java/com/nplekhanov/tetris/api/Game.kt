package com.nplekhanov.tetris.api

interface Game {
    fun createProcess(device: Device): GameProcess
    val name: String
}