package com.nplekhanov.tetris.api

class Vec2i(val x: Int, val y: Int) {
    companion object {
        val E = Vec2i(1, 0)
        val NE = Vec2i(1, 1)
        val N = Vec2i(0, 1)
        val NW = Vec2i(-1, 1)
        val W = Vec2i(-1, 0)
        val SW = Vec2i(-1, -1)
        val S = Vec2i(0, -1)
        val SE = Vec2i(1, -1)
        val ZERO = Vec2i(0, 0)
    }

    operator fun plus(other: Vec2i): Vec2i {
        return Vec2i(x + other.x, y + other.y)
    }

    fun qrot(quarters: Int): Vec2i {
        if (quarters == 0) {
            return this;
        }
        if (quarters < 0) {
            return qrot(quarters + 4)
        }
        return Vec2i(-y, x).qrot(quarters - 1)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vec2i

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }

    override fun toString(): String {
        return "Vec2i(x=$x, y=$y)"
    }

    operator fun times(value: Int): Vec2i {
        return Vec2i(x * value, y * value)
    }

    operator fun minus(value: Vec2i): Vec2i {
        return Vec2i(x - value.x, y - value.y)
    }
}