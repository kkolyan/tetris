package com.nplekhanov.tetris.api

import com.badlogic.gdx.Input

interface GameProcess {
    fun update(deltaTime: Float)
    fun isOver(): Boolean
}