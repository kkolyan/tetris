package com.nplekhanov.tetris.api

interface Device {
    val width: Int
    val height: Int
    fun drawPixel(x:Int, y:Int)
    fun isButtonPressed(button: Button): Boolean
    fun isButtonJustPressed(button: Button): Boolean
}