package com.nplekhanov.tetris.games.snake

import com.nplekhanov.tetris.api.*
import java.util.*
import kotlin.random.Random

class SnakeGame : Game {

    override val name: String get() = "Snake"

    override fun createProcess(device: Device): GameProcess {
        return SnakeGameProcess(device)
    }

    class SnakeGameProcess(private val device: Device) : GameProcess {

        override fun isOver(): Boolean = gameOver

        private val fuelPerCell = 0.25f;
        var gameOver = false

        private val snakeSegments = ArrayDeque<Vec2i>(1024)

        init {
            snakeSegments.addFirst(randomPoint())
        }

        private val foodChunks: MutableSet<Vec2i> = hashSetOf()

        private fun randomPoint(): Vec2i = Vec2i(Random.nextInt(device.width), Random.nextInt(device.height))

        private var fuel: Float = 0f
        private var direction = listOf(Vec2i.N, Vec2i.W, Vec2i.S, Vec2i.E).random()
        private var lastAppliedDirection: Vec2i = direction

        private val directionByKey = linkedMapOf(
                Button.LEFT to Vec2i.W,
                Button.RIGHT to Vec2i.E,
                Button.UP to Vec2i.N,
                Button.DOWN to Vec2i.S
        )

        private fun normalizePoint(point: Vec2i): Vec2i {
            var x = point.x
            var y = point.y
            while (x < 0) x += device.width
            while (x >= device.width) x -= device.width
            while (y < 0) y += device.height
            while (y >= device.height) y -= device.height
            return Vec2i(x, y)
        }

        override fun update(deltaTime: Float) {
            if (!gameOver) {
                fuel += deltaTime

                directionByKey
                        .filter { device.isButtonJustPressed(it.key) }
                        .map { it.value }
                        .filter { it != lastAppliedDirection && it != Vec2i.ZERO - lastAppliedDirection }
                        .forEach { direction = it }

                while (foodChunks.isEmpty()) {
                    val point = randomPoint()
                    if (!snakeSegments.contains(point)) {
                        foodChunks += point
                    }
                }

                while (fuel > fuelPerCell) {
                    val next = normalizePoint(snakeSegments.first + direction)
                    lastAppliedDirection = direction
                    if (snakeSegments.contains(next)) {
                        gameOver = true
                        return
                    }
                    if (foodChunks.contains(next)) {
                        foodChunks -= next
                    } else {
                        snakeSegments.removeLast()
                    }
                    snakeSegments.addFirst(next)

                    fuel -= fuelPerCell
                }
            }

            for (chunk in foodChunks) {
                device.drawPixel(chunk.x, chunk.y)
            }
            for (segment in snakeSegments) {
                device.drawPixel(segment.x, segment.y)
            }
        }
    }

}
