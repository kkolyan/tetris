package com.nplekhanov.tetris.games.tetris

import com.nplekhanov.tetris.api.Vec2i

open class Figure private constructor(
        val points: Set<Vec2i>,
        private val origin: Vec2i
) {
    constructor(vararg points: Vec2i) : this(points.toSet(), Vec2i.ZERO)

    fun qrot(quarters: Int): Figure {
        return Figure(points.map { (it - origin).qrot(quarters) + origin }.toSet(), origin)
    }

    operator fun plus(vector: Vec2i): Figure {
        return Figure(points.map { it + vector }.toSet(), origin + vector)
    }

    operator fun minus(vector: Vec2i): Figure {
        return this + (Vec2i.ZERO - vector)
    }
}