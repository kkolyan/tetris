package com.nplekhanov.tetris.games.tetris

import com.nplekhanov.tetris.api.Vec2i

object Figures {
    val Cube = Figure(
            Vec2i.ZERO,
            Vec2i.S,
            Vec2i.E,
            Vec2i.SE
    )

    val Lightning = Figure(Vec2i.W,
            Vec2i.ZERO,
            Vec2i.S,
            Vec2i.SE
    )

    val Line = Figure(
            Vec2i.W,
            Vec2i.ZERO,
            Vec2i.E,
            Vec2i.E * 2
    )

    val Podium = Figure(
            Vec2i.W,
            Vec2i.ZERO,
            Vec2i.E,
            Vec2i.S
    )

    val ReverseLightning = Figure(Vec2i.E,
            Vec2i.ZERO,
            Vec2i.S,
            Vec2i.SW
    )
}