package com.nplekhanov.tetris.games.tetris

import com.nplekhanov.tetris.api.*

class TetrisGame : Game {

    override val name: String get() = "Tetris"

    override fun createProcess(device: Device): GameProcess = TetrisGameProcess(device)

    class TetrisGameProcess(private val device: Device) : GameProcess {


        override fun isOver(): Boolean = gameOver

        private var secondsPerCell: Float = 0.5f
        private val field: MutableSet<Vec2i> = HashSet()
        private val figures = setOf(
                Figures.Cube,
                Figures.Lightning,
                Figures.ReverseLightning,
                Figures.Line,
                Figures.Podium
        )
        var gameOver: Boolean = false

        var movingFigure: Figure? = null

        private var nonSpentTime: Float = 0.0f

        private fun tryMoveFigure(delta: Vec2i): Boolean {
            val moved = movingFigure!! + delta
            if (moved.points.any { field.contains(it) || it.y >= device.height || it.y < 0 || it.x < 0 || it.x >= device.width }) {
                return false
            }
            movingFigure!!.points.forEach { field -= it }
            movingFigure = moved
            return true
        }


        override fun update(deltaTime: Float) {

            if (!gameOver && movingFigure == null) {
                movingFigure = figures.random()
                movingFigure = movingFigure!! + Vec2i(device.width / 2, device.height - 1)
                if (movingFigure!!.points.any { field.contains(it) }) {
                    gameOver = true
                }
            }
            if (!gameOver) {

                if (device.isButtonJustPressed(Button.UP)) {
                    movingFigure = movingFigure!!.qrot(1)
                }
                if (device.isButtonJustPressed(Button.LEFT)) {
                    tryMoveFigure(Vec2i.W)
                }
                if (device.isButtonJustPressed(Button.RIGHT)) {
                    tryMoveFigure(Vec2i.E)
                }
                nonSpentTime += deltaTime

                while (nonSpentTime > secondsPerCell) {
                    moveFigure()
                    nonSpentTime -= secondsPerCell
                }

                while (device.isButtonJustPressed(Button.DOWN) && movingFigure != null) {
                    moveFigure()
                }
            }

            val renderSolidCells = { cells: Set<Vec2i> ->
                cells.forEach { cell ->
                    device.drawPixel(cell.x, cell.y)
                }
            }
            renderSolidCells(field)
            if (movingFigure != null) {
                renderSolidCells(movingFigure!!.points)
            }
        }

        private fun moveFigure() {
            if (!tryMoveFigure(Vec2i.S)) {
                movingFigure!!.points.forEach { field += it }
                val affectedRows = movingFigure!!.points.map { it.y }

                var i = affectedRows.min()!!
                var to = affectedRows.max()!!
                while (i <= to) {
                    if ((0 until device.width).all { x -> field.contains(Vec2i(x, i)) }) {
                        (0 until device.width).forEach { x -> field -= Vec2i(x, i) }
                        (i until device.height).forEach { y ->
                            (0 until device.width).forEach { x ->
                                if (field.remove(Vec2i(x, y + 1))) {
                                    field += Vec2i(x, y)
                                }
                            }
                        }
                        to--
                    } else {
                        i++
                    }
                }

                movingFigure = null
            }
        }
    }
}