package com.nplekhanov.tetris

import com.nplekhanov.tetris.api.Vec2i
import org.junit.Assert.*
import org.junit.Test

class Vec2iTest {
    @Test
    fun testRot() {
        assertEquals(Vec2i.E, Vec2i.N.qrot(1))
        assertEquals(Vec2i.S, Vec2i.N.qrot(2))
        assertEquals(Vec2i.W, Vec2i.N.qrot(3))
        assertEquals(Vec2i.N, Vec2i.N.qrot(4))
    }
}